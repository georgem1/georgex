.. _architecture:

============
Architecture
============

.. sidebar:: Further reading

   - Reverse Proxy: :ref:`Apache <apache georgex site>` & :ref:`nginx <nginx
     georgex site>`
   - uWSGI: :ref:`georgex uwsgi`
   - GeorgeX: :ref:`installation basic`

Herein you will find some hints and suggestions about typical architectures of
GeorgeX infrastructures.

.. _architecture uWSGI:

uWSGI Setup
===========

We start with a *reference* setup for public GeorgeX instances which can be build
up and maintained by the scripts from our :ref:`toolboxing`.

.. _arch public:

.. kernel-figure:: arch_public.dot
   :alt: arch_public.dot

   Reference architecture of a public GeorgeX setup.

The reference installation activates ``server.limiter``, ``server.image_proxy``
and ``ui.static_use_hash`` (:origin:`/etc/georgex/settings.yml
<utils/templates/etc/georgex/settings.yml>`)

.. literalinclude:: ../../utils/templates/etc/georgex/settings.yml
   :language: yaml
   :end-before: # preferences:
