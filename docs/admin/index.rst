===========================
Administrator documentation
===========================

.. toctree::
   :maxdepth: 2
   :caption: Contents

   installation
   installation-docker
   installation-scripts
   installation-georgex
   installation-uwsgi
   installation-nginx
   installation-apache
   update-georgex
   engines/index
   api
   architecture
   plugins
   buildhosts
