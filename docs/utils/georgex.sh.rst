
.. _georgex.sh:

====================
``utils/georgex.sh``
====================

.. sidebar:: further reading

   - :ref:`architecture`
   - :ref:`installation`
   - :ref:`installation nginx`
   - :ref:`installation apache`

To simplify the installation and maintenance of a GeorgeX instance you can use the
script :origin:`utils/georgex.sh`.

Install
=======

In most cases you will install GeorgeX simply by running the command:

.. code::  bash

   sudo -H ./utils/searx.sh install all

The installation is described in chapter :ref:`installation basic`.

.. _georgex.sh overview:

Overview
========

The ``--help`` output of the script is largely self-explanatory:

.. program-output:: ../utils/georgex.sh --help
