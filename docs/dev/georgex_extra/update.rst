=========================
``georgex_extra/update/``
=========================

:origin:`[source] <georgex_extra/update/__init__.py>`

Scripts to update static data in :origin:`searx/data/`

.. _update_ahmia_blacklist.py:

``update_ahmia_blacklist.py``
=============================

:origin:`[source] <georgex_extra/update/update_ahmia_blacklist.py>`

.. automodule:: georgex_extra.update.update_ahmia_blacklist
  :members:


``update_currencies.py``
========================

:origin:`[source] <georgex_extra/update/update_currencies.py>`

.. automodule:: georgex_extra.update.update_currencies
  :members:

``update_engine_descriptions.py``
=================================

:origin:`[source] <georgex_extra/update/update_engine_descriptions.py>`

.. automodule:: georgex_extra.update.update_engine_descriptions
  :members:


``update_external_bangs.py``
============================

:origin:`[source] <georgex_extra/update/update_external_bangs.py>`

.. automodule:: georgex_extra.update.update_external_bangs
  :members:


``update_firefox_version.py``
=============================

:origin:`[source] <georgex_extra/update/update_firefox_version.py>`

.. automodule:: georgex_extra.update.update_firefox_version
  :members:


``update_languages.py``
=======================

:origin:`[source] <georgex_extra/update/update_languages.py>`

.. automodule:: georgex_extra.update.update_languages
  :members:


``update_osm_keys_tags.py``
===========================

:origin:`[source] <georgex_extra/update/update_osm_keys_tags.py>`

.. automodule:: georgex_extra.update.update_osm_keys_tags
  :members:


``update_pygments.py``
======================

:origin:`[source] <georgex_extra/update/update_pygments.py>`

.. automodule:: georgex_extra.update.update_pygments
  :members:


``update_wikidata_units.py``
============================

:origin:`[source] <georgex_extra/update/update_wikidata_units.py>`

.. automodule:: georgex_extra.update.update_wikidata_units
  :members:
