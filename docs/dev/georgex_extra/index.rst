.. _georgex_extra:

=============================
Tooling box ``georgex_extra``
=============================

In the folder :origin:`georgex_extra/` we maintain some tools useful for CI and
developers.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   update
   standalone_searx.py
