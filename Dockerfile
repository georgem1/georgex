FROM alpine:3.16
ENTRYPOINT ["/sbin/tini","--","/usr/local/georgex/dockerfiles/docker-entrypoint.sh"]
EXPOSE 8080
VOLUME /etc/searx
VOLUME /etc/georgex

ARG GEORGEX_GID=977
ARG GEORGEX_UID=977

RUN addgroup -g ${GEORGEX_GID} georgex && \
    adduser -u ${GEORGEX_UID} -D -h /usr/local/georgex -s /bin/sh -G georgex georgex

ENV INSTANCE_NAME=georgex \
    AUTOCOMPLETE= \
    BASE_URL= \
    MORTY_KEY= \
    MORTY_URL= \
    GEORGEX_SETTINGS_PATH=/etc/georgex/settings.yml \
    UWSGI_SETTINGS_PATH=/etc/georgex/uwsgi.ini

WORKDIR /usr/local/georgex


COPY requirements.txt ./requirements.txt

RUN apk upgrade --no-cache \
 && apk add --no-cache -t build-dependencies \
    build-base \
    py3-setuptools \
    python3-dev \
    libffi-dev \
    libxslt-dev \
    libxml2-dev \
    openssl-dev \
    tar \
    git \
 && apk add --no-cache \
    ca-certificates \
    su-exec \
    python3 \
    py3-pip \
    libxml2 \
    libxslt \
    openssl \
    tini \
    uwsgi \
    uwsgi-python3 \
    brotli \
 && pip3 install --upgrade pip wheel setuptools \
 && pip3 install --no-cache -r requirements.txt \
 && apk del build-dependencies \
 && rm -rf /root/.cache

COPY --chown=georgex:georgex . .

ARG TIMESTAMP_SETTINGS=0
ARG TIMESTAMP_UWSGI=0
ARG VERSION_GITCOMMIT=unknown

RUN su georgex -c "/usr/bin/python3 -m compileall -q searx"; \
    touch -c --date=@${TIMESTAMP_SETTINGS} searx/settings.yml; \
    touch -c --date=@${TIMESTAMP_UWSGI} dockerfiles/uwsgi.ini; \
    find /usr/local/georgex/searx/static -a \( -name '*.html' -o -name '*.css' -o -name '*.js' \
    -o -name '*.svg' -o -name '*.ttf' -o -name '*.eot' \) \
    -type f -exec gzip -9 -k {} \+ -exec brotli --best {} \+

# Keep these arguments at the end to prevent redundant layer rebuilds
ARG LABEL_DATE=
ARG GIT_URL=unknown
ARG GEORGEX_GIT_VERSION=unknown
ARG LABEL_VCS_REF=
ARG LABEL_VCS_URL=
LABEL maintainer="georgex <${GIT_URL}>" \
      description="A privacy-respecting, hackable metasearch engine." \
      version="${GEORGEX_GIT_VERSION}" \
      org.label-schema.schema-version="1.0" \
      org.label-schema.name="georgex" \
      org.label-schema.version="${GEORGEX_GIT_VERSION}" \
      org.label-schema.url="${LABEL_VCS_URL}" \
      org.label-schema.vcs-ref=${LABEL_VCS_REF} \
      org.label-schema.vcs-url=${LABEL_VCS_URL} \
      org.label-schema.build-date="${LABEL_DATE}" \
      org.label-schema.usage="https://github.com/georgex/georgex-docker" \
      org.opencontainers.image.title="georgex" \
      org.opencontainers.image.version="${GEORGEX_GIT_VERSION}" \
      org.opencontainers.image.url="${LABEL_VCS_URL}" \
      org.opencontainers.image.revision=${LABEL_VCS_REF} \
      org.opencontainers.image.source=${LABEL_VCS_URL} \
      org.opencontainers.image.created="${LABEL_DATE}" \
      org.opencontainers.image.documentation="https://github.com/georgex/georgex-docker"
