# Security Policy

We love responsible reports of (potential) security issues in GeorgeX.

You can contact us at security@georgex.org.

Be sure to provide as much information as possible and if found
also reproduction steps of the identified vulnerability. Also
add the specific URL of the project as well as code you found
the issue in to your report.
