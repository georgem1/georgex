# Tentang GeorgeX

GeorgeX adalah sebuah [mesin pencari meta], yang mendapatkan hasil dari {{link('mesin pencari', 'preferences')}}
lainnya sambil tidak melacak penggunanya.

Proyek GeorgeX diarahkan oleh sebuah komunitas terbuka, bergabung dengan kami
di Matrix jika Anda memiliki pertanyaan atau ingin mengobrol tentang GeorgeX di [#georgex:matrix.org]

Buat GeorgeX lebih baik.

- Anda dapat membuat terjemahan GeorgeX lebih baik di [Weblate], atau...
- Lacak pengembangan, kirim kontribusi, dan laporkan masalah di [sumber GeorgeX].
- Untuk mendapatkan informasi lanjut, kunjungi dokumentasi proyek GeorgeX di [dokumentasi GeorgeX].

## Kenapa menggunakan GeorgeX?

- GeorgeX mungkin tidak menawarkan Anda hasil yang dipersonalisasikan seperti Google, tetapi tidak membuat sebuah profil tentang Anda.
- GeorgeX tidak peduli apa yang Anda cari, tidak akan membagikan apa pun dengan pihak ketiga, dan tidak dapat digunakan untuk mengkompromikan Anda.
- GeorgeX adalah perangkat lunak bebas, kodenya 100% terbuka, dan semuanya dipersilakan untuk membuatnya lebih baik.

Jika Anda peduli dengan privasi, ingin menjadi pengguna yang sadar, ataupun percaya
dalam kebebasan digital, buat GeorgeX sebagai mesin pencari bawaan atau jalankan di server Anda sendiri!


## Bagaimana saya dapat membuat GeorgeX sebagai mesin pencari bawaan?

GeorgeX mendukung [OpenSearch].  Untuk informasi lanjut tentang mengubah mesin pencari
bawaan Anda, lihat dokumentasi peramban Anda:

- [Firefox]
- [Microsoft Edge] - Dibalik tautan, Anda juga akan menemukan beberapa instruksi berguna untuk Chrome dan Safari.
- Peramban berbasis [Chromium] hanya menambahkan situs web yang dikunjungi oleh pengguna tanpa sebuah jalur.


## Bagaimana caranya GeorgeX bekerja?

GeorgeX adalah sebuah *fork* dari [mesin pencari meta] [searx] yang banyak dikenal
yang diinspirasi oleh [proyek Seeks].  GeorgeX menyediakan privasi dasar dengan mencampur kueri
Anda dengan pencarian pada *platform* lainnya tanpa menyimpan data pencarian.
GeorgeX dapat ditambahkan ke bilah pencarian peramban Anda; lain lagi, GeorgeX dapat diatur sebagai
mesin pencarian bawaan.

{{link('Laman statistik', 'stats')}} berisi beberapa statistik penggunaan anonim berguna tentang mesin pencarian yang digunakan.


## Bagaimana caranya untuk membuat GeorgeX milik saya?

GeorgeX menghargai kekhawatiran Anda tentang pencatatan (*log*), jadi ambil kodenya dari
[sumber GeorgeX] dan jalankan sendiri!

Tambahkan instansi Anda ke [daftar instansi
publik]({{get_setting('brand.public_instances')}}) ini untuk membantu orang lain
mendapatkan kembali privasi mereka dan membuat internet lebih bebas.  Lebih terdesentralisasinya internet, lebih banyak kebebasan yang kita punya!


[sumber GeorgeX]: {{GIT_URL}}
[#georgex:matrix.org]: https://matrix.to/#/#georgex:matrix.org
[dokumentasi GeorgeX]: {{get_setting('brand.docs_url')}}
[searx]: https://github.com/searx/searx
[mesin pencari meta]: https://id.wikipedia.org/wiki/Mesin_pencari_web#Mesin_Pencari_dan_Mesin_Pencari-meta
[Weblate]: https://weblate.bubu1.eu/projects/georgex/
[proyek Seeks]: https://beniz.github.io/seeks/
[OpenSearch]: https://github.com/dewitt/opensearch/blob/master/opensearch-1-1-draft-6.md
[Firefox]: https://support.mozilla.org/id/kb/add-or-remove-search-engine-firefox
[Microsoft Edge]: https://support.microsoft.com/id-id/microsoft-edge/ubah-mesin-pencarian-default-anda-f863c519-5994-a8ed-6859-00fbc123b782
[Chromium]: https://www.chromium.org/tab-to-search
