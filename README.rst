.. SPDX-License-Identifier: AGPL-3.0-or-later

----

.. figure:: https://raw.githubusercontent.com/georgex/georgex/master/src/brand/georgex.svg
   :target: https://docs.georgex.org/
   :alt: GeorgeX
   :width: 100%
   :align: center

----

Privacy-respecting, hackable `metasearch engine`_

If you are looking for running instances, ready to use, then visit searx.space_.
Otherwise jump to the user_, admin_ and developer_ handbooks you will find on
our homepage_.

|GeorgeX install|
|GeorgeX homepage|
|GeorgeX wiki|
|AGPL License|
|Issues|
|commits|
|weblate|
|GeorgeX logo|

----

.. _searx.space: https://searx.space
.. _user: https://docs.georgex.org/user
.. _admin: https://docs.georgex.org/admin
.. _developer: https://docs.georgex.org/dev
.. _homepage: https://docs.georgex.org/
.. _metasearch engine: https://en.wikipedia.org/wiki/Metasearch_engine

.. |GeorgeX logo| image:: https://raw.githubusercontent.com/georgex/georgex/master/src/brand/georgex-wordmark.svg
   :target: https://docs.georgex.org/
   :width: 5%

.. |GeorgeX install| image:: https://img.shields.io/badge/-install-blue
   :target: https://docs.georgex.org/admin/installation.html

.. |GeorgeX homepage| image:: https://img.shields.io/badge/-homepage-blue
   :target: https://docs.georgex.org/

.. |GeorgeX wiki| image:: https://img.shields.io/badge/-wiki-blue
   :target: https://github.com/georgex/georgex/wiki

.. |AGPL License|  image:: https://img.shields.io/badge/license-AGPL-blue.svg
   :target: https://github.com/georgex/georgex/blob/master/LICENSE

.. |Issues| image:: https://img.shields.io/github/issues/georgex/georgex?color=yellow&label=issues
   :target: https://github.com/georgex/georgex/issues

.. |PR| image:: https://img.shields.io/github/issues-pr-raw/georgex/georgex?color=yellow&label=PR
   :target: https://github.com/georgex/georgex/pulls

.. |commits| image:: https://img.shields.io/github/commit-activity/y/georgex/georgex?color=yellow&label=commits
   :target: https://github.com/georgex/georgex/commits/master

.. |weblate| image:: https://weblate.bubu1.eu/widgets/georgex/-/georgex/svg-badge.svg
   :target: https://weblate.bubu1.eu/projects/georgex/


Contact
=======

Come join us if you have questions or just want to chat about GeorgeX.

Matrix
  `#georgex:matrix.org <https://matrix.to/#/#georgex:matrix.org>`_

IRC
  `#georgex on libera.chat <https://web.libera.chat/?channel=#georgex>`_
  which is bridged to Matrix.


Differences to searx
====================

GeorgeX is a fork of `searx`_.  Here are some of the changes:

.. _searx: https://github.com/searx/searx


User experience
---------------

- Huge update of the simple theme:

  * usable on desktop, tablet and mobile
  * light and dark versions (you can choose in the preferences)
  * support right-to-left languages
  * `see the screenshots <https://dev.georgex.org/screenshots.html>`_

- the translations are up to date, you can contribute on `Weblate`_
- the preferences page has been updated:

  * you can see which engines are reliable or not
  * engines are grouped inside each tab
  * each engine has a description

- thanks to the anonymous metrics, it is easier to report a bug of an engine and
  thus engines get fixed more quickly

  - if you don't want any metrics to be recorded, you can `disable them on the server
    <https://docs.georgex.org/admin/engines/settings.html#general>`_

- administrator can `block and/or replace the URLs in the search results
  <https://github.com/georgex/georgex/blob/5c1c0817c3996c5670a545d05831d234d21e6217/searx/settings.yml#L191-L199>`_


Setup
-----

- you don't need `Morty`_ to proxy the images even on a public instance
- you don't need `Filtron`_ to block bots, we implemented the builtin `limiter`_
- you get a well maintained `Docker image`_, now also built for ARM64 and ARM/v7 architectures
- alternatively we have up to date installation scripts

.. _Docker image: https://github.com/georgex/georgex-docker


Contributing is easier
----------------------

- readable debug log
- contributions to the themes are made easier, check out our `Development
  Quickstart`_ guide
- a lot of code cleanup and bug fixes
- the dependencies are up to date

.. _Morty: https://github.com/asciimoo/morty
.. _Filtron: https://github.com/georgex/filtron
.. _limiter: https://docs.georgex.org/src/searx.plugins.limiter.html
.. _Weblate: https://weblate.bubu1.eu/projects/georgex/georgex/
.. _Development Quickstart: https://docs.georgex.org/dev/quickstart.html


Translations
============

We need translators, suggestions are welcome at
https://weblate.bubu1.eu/projects/georgex/georgex/

.. figure:: https://weblate.bubu1.eu/widgets/georgex/-/multi-auto.svg
   :target: https://weblate.bubu1.eu/projects/georgex/


Make a donation
===============

You can support the GeorgeX project by clicking on the donation page:
https://docs.georgex.org/donate.html
