#!/usr/bin/env bash
# SPDX-License-Identifier: AGPL-3.0-or-later
# shellcheck disable=SC2001

# Script options from the environment:
GEORGEX_UWSGI_USE_SOCKET="${GEORGEX_UWSGI_USE_SOCKET:-true}"

# shellcheck source=utils/lib.sh
source "$(dirname "${BASH_SOURCE[0]}")/lib.sh"
# shellcheck source=utils/lib_redis.sh
source "$(dirname "${BASH_SOURCE[0]}")/lib_redis.sh"
# shellcheck source=utils/brand.env
source "${REPO_ROOT}/utils/brand.env"

SERVICE_NAME="georgex"
SERVICE_USER="georgex"
SERVICE_HOME="/usr/local/georgex"
SERVICE_GROUP="georgex"

GEORGEX_SRC="${SERVICE_HOME}/georgex-src"
# shellcheck disable=SC2034
GEORGEX_STATIC="${GEORGEX_SRC}/searx/static"

GEORGEX_PYENV="${SERVICE_HOME}/searx-pyenv"
GEORGEX_SETTINGS_PATH="/etc/georgex/settings.yml"
GEORGEX_UWSGI_APP="georgex.ini"

GEORGEX_INTERNAL_HTTP="${GEORGEX_BIND_ADDRESS}:${GEORGEX_PORT}"
if [[ ${GEORGEX_UWSGI_USE_SOCKET} == true ]]; then
    GEORGEX_UWSGI_SOCKET="${SERVICE_HOME}/run/socket"
else
    GEORGEX_UWSGI_SOCKET=
fi

# GEORGEX_URL: the public URL of the instance (https://example.org/georgex).  The
# value is taken from environment ${GEORGEX_URL} in ./utils/brand.env.  This
# variable is an empty string if server.base_url in the settings.yml is set to
# 'false'.

GEORGEX_URL="${GEORGEX_URL:-http://$(uname -n)/georgex}"
GEORGEX_URL="${GEORGEX_URL%/}" # if exists, remove trailing slash
if in_container; then
    # hint: Linux containers do not have DNS entries, lets use IPs
    GEORGEX_URL="http://$(primary_ip)/georgex"
fi
GEORGEX_URL_PATH="$(echo "${GEORGEX_URL}" | sed -e 's,^.*://[^/]*\(/.*\),\1,g')"
[[ "${GEORGEX_URL_PATH}" == "${GEORGEX_URL}" ]] && GEORGEX_URL_PATH=/

# Apache settings

APACHE_GEORGEX_SITE="georgex.conf"

# nginx settings

NGINX_GEORGEX_SITE="georgex.conf"

# apt packages

GEORGEX_PACKAGES_debian="\
python3-dev python3-babel python3-venv
uwsgi uwsgi-plugin-python3
git build-essential libxslt-dev zlib1g-dev libffi-dev libssl-dev"

GEORGEX_BUILD_PACKAGES_debian="\
firefox graphviz imagemagick texlive-xetex librsvg2-bin
texlive-latex-recommended texlive-extra-utils fonts-dejavu
latexmk shellcheck"

# pacman packages

GEORGEX_PACKAGES_arch="\
python python-pip python-lxml python-babel
uwsgi uwsgi-plugin-python
git base-devel libxml2"

GEORGEX_BUILD_PACKAGES_arch="\
firefox graphviz imagemagick texlive-bin extra/librsvg
texlive-core texlive-latexextra ttf-dejavu shellcheck"

# dnf packages

GEORGEX_PACKAGES_fedora="\
python python-pip python-lxml python-babel python3-devel
uwsgi uwsgi-plugin-python3
git @development-tools libxml2 openssl"

GEORGEX_BUILD_PACKAGES_fedora="\
firefox graphviz graphviz-gd ImageMagick librsvg2-tools
texlive-xetex-bin texlive-collection-fontsrecommended
texlive-collection-latex dejavu-sans-fonts dejavu-serif-fonts
dejavu-sans-mono-fonts ShellCheck"

case $DIST_ID-$DIST_VERS in
    ubuntu-18.04)
        GEORGEX_PACKAGES="${GEORGEX_PACKAGES_debian}"
        GEORGEX_BUILD_PACKAGES="${GEORGEX_BUILD_PACKAGES_debian}"
        APACHE_PACKAGES="$APACHE_PACKAGES libapache2-mod-proxy-uwsgi"
        ;;
    ubuntu-20.04)
        # https://wiki.ubuntu.com/FocalFossa/ReleaseNotes#Python3_by_default
        GEORGEX_PACKAGES="${GEORGEX_PACKAGES_debian} python-is-python3"
        GEORGEX_BUILD_PACKAGES="${GEORGEX_BUILD_PACKAGES_debian}"
        ;;
    ubuntu-*|debian-*)
        GEORGEX_PACKAGES="${GEORGEX_PACKAGES_debian}"
        GEORGEX_BUILD_PACKAGES="${GEORGEX_BUILD_PACKAGES_debian}"
        ;;
    arch-*)
        GEORGEX_PACKAGES="${GEORGEX_PACKAGES_arch}"
        GEORGEX_BUILD_PACKAGES="${GEORGEX_BUILD_PACKAGES_arch}"
        ;;
    fedora-*)
        GEORGEX_PACKAGES="${GEORGEX_PACKAGES_fedora}"
        GEORGEX_BUILD_PACKAGES="${GEORGEX_BUILD_PACKAGES_fedora}"
        ;;
esac

_service_prefix="  ${_Yellow}|${SERVICE_USER}|${_creset} "

# ----------------------------------------------------------------------------
usage() {
# ----------------------------------------------------------------------------

    # shellcheck disable=SC1117
    cat <<EOF
usage:
  $(basename "$0") install    [all|user|pyenv|settings|uwsgi|redis|nginx|apache|georgex-src|packages|buildhost]
  $(basename "$0") remove     [all|user|pyenv|settings|uwsgi|redis|nginx|apache]
  $(basename "$0") instance   [cmd|update|check|localtest|inspect]
install|remove:
  all           : complete (de-) installation of the GeorgeX service
  user          : service user '${SERVICE_USER}' (${SERVICE_HOME})
  pyenv         : virtualenv (python) in ${GEORGEX_PYENV}
  settings      : settings from ${GEORGEX_SETTINGS_PATH}
  uwsgi         : GeorgeX's uWSGI app ${GEORGEX_UWSGI_APP}
  redis         : build & install or remove a local redis server ${REDIS_HOME}/run/redis.sock
  nginx         : HTTP site ${NGINX_APPS_AVAILABLE}/${NGINX_GEORGEX_SITE}
  apache        : HTTP site ${APACHE_SITES_AVAILABLE}/${APACHE_GEORGEX_SITE}
install:
  georgex-src   : clone ${GIT_URL} into ${GEORGEX_SRC}
  packages      : installs packages from OS package manager required by GeorgeX
  buildhost     : installs packages from OS package manager required by a GeorgeX buildhost
instance:
  update        : update GeorgeX instance (git fetch + reset & update settings.yml)
  check         : run checks from utils/georgex_check.py in the active installation
  inspect       : run some small tests and inspect GeorgeX's server status and log
  get_setting   : get settings value from running GeorgeX instance
  cmd           : run command in GeorgeX instance's environment (e.g. bash)
EOF
    georgex.instance.env
    [[ -n ${1} ]] &&  err_msg "$1"
}

georgex.instance.env() {
    echo "uWSGI:"
    if [[ ${GEORGEX_UWSGI_USE_SOCKET} == true ]]; then
        echo "  GEORGEX_UWSGI_SOCKET : ${GEORGEX_UWSGI_SOCKET}"
    else
        echo "  GEORGEX_INTERNAL_HTTP: ${GEORGEX_INTERNAL_HTTP}"
    fi
    cat <<EOF
environment ${GEORGEX_SRC}/utils/brand.env:
  GIT_URL              : ${GIT_URL}
  GIT_BRANCH           : ${GIT_BRANCH}
  GEORGEX_URL          : ${GEORGEX_URL}
  GEORGEX_PORT         : ${GEORGEX_PORT}
  GEORGEX_BIND_ADDRESS : ${GEORGEX_BIND_ADDRESS}
EOF
}

main() {
    required_commands \
        sudo systemctl install git wget curl \
        || exit

    local _usage="unknown or missing $1 command $2"

    case $1 in
        --getenv)  var="$2"; echo "${!var}"; exit 0;;
        -h|--help) usage; exit 0;;
        install)
            sudo_or_exit
            case $2 in
                all) georgex.install.all;;
                user) georgex.install.user;;
                pyenv) georgex.install.pyenv;;
                georgex-src) georgex.install.clone;;
                settings) georgex.install.settings;;
                uwsgi) georgex.install.uwsgi;;
                packages) georgex.install.packages;;
                buildhost) georgex.install.buildhost;;
                nginx) georgex.nginx.install;;
                apache) georgex.apache.install;;
                redis) georgex.install.redis;;
                *) usage "$_usage"; exit 42;;
            esac
            ;;
        remove)
            sudo_or_exit
            case $2 in
                all) georgex.remove.all;;
                user) drop_service_account "${SERVICE_USER}";;
                pyenv) georgex.remove.pyenv;;
                settings) georgex.remove.settings;;
                uwsgi) georgex.remove.uwsgi;;
                apache) georgex.apache.remove;;
                remove) georgex.nginx.remove;;
                redis) georgex.remove.redis;;
                *) usage "$_usage"; exit 42;;
            esac
            ;;
        instance)
            case $2 in
                update)
                    sudo_or_exit
                    georgex.instance.update
                    ;;
                check)
                    sudo_or_exit
                    georgex.instance.self.call georgex.check
                    ;;
                inspect)
                    sudo_or_exit
                    georgex.instance.inspect
                    ;;
                cmd)
                    sudo_or_exit
                    shift; shift; georgex.instance.exec "$@"
                    ;;
                get_setting)
                    shift; shift; georgex.instance.get_setting "$@"
                    ;;
                call)
                    # call a function in instance's environment
                    shift; shift; georgex.instance.self.call "$@"
                    ;;
                _call)
                    shift; shift; "$@"
                    ;;
                *) usage "$_usage"; exit 42;;
            esac
            ;;
        *)
            local cmd="$1"
            _type="$(type -t "$cmd")"
            if [ "$_type" != 'function' ]; then
                usage "unknown or missing command $1"
                exit 42
            else
                "$cmd" "$@"
            fi
            ;;
    esac
}

georgex.install.all() {
    rst_title "GeorgeX installation" part

    local redis_url

    rst_title "GeorgeX"
    georgex.install.packages
    wait_key 10
    georgex.install.user
    wait_key 10
    georgex.install.clone
    wait_key
    georgex.install.pyenv
    wait_key
    georgex.install.settings
    wait_key
    georgex.instance.localtest
    wait_key
    georgex.install.uwsgi
    wait_key

    rst_title "Redis DB"
    georgex.install.redis.db

    rst_title "HTTP Server"
    georgex.install.http.site

    rst_title "Finalize installation"
    if ask_yn "Do you want to run some checks?" Yn; then
        georgex.instance.self.call georgex.check
    fi
}

georgex.install.redis.db() {
    local redis_url

    redis_url=$(georgex.instance.get_setting redis.url)
    rst_para "\
In your instance, redis DB connector is configured at:

    ${redis_url}
"
    if georgex.instance.exec python -c "from searx.shared import redisdb; redisdb.init() or exit(42)"; then
        info_msg "GeorgeX instance is able to connect redis DB."
        return
    fi
    if ! [[ ${redis_url} = unix://${REDIS_HOME}/run/redis.sock* ]]; then
        err_msg "GeorgeX instance can't connect redis DB / check redis & your settings"
        return
    fi
    rst_para ".. but this redis DB is not installed yet."

    case $DIST_ID-$DIST_VERS in
        fedora-*)
            # Fedora runs uWSGI in emperor-tyrant mode: in Tyrant mode the
            # Emperor will run the vassal using the UID/GID of the vassal
            # configuration file [1] (user and group of the app .ini file).
            #
            # HINT: without option ``emperor-tyrant-initgroups=true`` in
            # ``/etc/uwsgi.ini`` the process won't get the additional groups,
            # but this option is not available in 2.0.x branch [2][3] / on
            # fedora35 there is v2.0.20 installed --> no way to get additional
            # groups on fedora's tyrant mode.
            #
            # ERROR:searx.shared.redis: [georgex (993)] can't connect redis DB ...
            # ERROR:searx.shared.redis:   Error 13 connecting to unix socket: /usr/local/georgex-redis/run/redis.sock. Permission denied.
            # ERROR:searx.plugins.limiter: init limiter DB failed!!!
            #
            # $ ps -aef | grep '/usr/sbin/uwsgi --ini georgex.ini'
            # georgex       93      92  0 12:43 ?        00:00:00 /usr/sbin/uwsgi --ini georgex.ini
            # georgex      186      93  0 12:44 ?        00:00:01 /usr/sbin/uwsgi --ini georgex.ini
            #
            # Additional groups:
            #
            # $ groups georgex
            # georgex : georgex georgex-redis
            #
            # Here you can see that the additional "Groups" of PID 186 are unset
            # (missing gid of georgex-redis)
            #
            # $ cat /proc/186/task/186/status
            # ...
            # Uid:      993     993     993     993
            # Gid:      993     993     993     993
            # FDSize:   128
            # Groups:
            # ...
            #
            # [1] https://uwsgi-docs.readthedocs.io/en/latest/Emperor.html#tyrant-mode-secure-multi-user-hosting
            # [2] https://github.com/unbit/uwsgi/issues/2099
            # [3] https://github.com/unbit/uwsgi/pull/752

            rst_para "\
Fedora uses emperor-tyrant mode / in this mode we had a lot of trouble with
sockets and permissions of the vasals.  We recommend to setup a redis DB
and using redis:// TCP protocol in the settings.yml configuration."
            ;;
        *)
            if ask_yn "Do you want to install the redis DB now?" Yn; then
                georgex.install.redis
                uWSGI_restart "$GEORGEX_UWSGI_APP"
            fi
            ;;
    esac
}

georgex.install.http.site() {

    if apache_is_installed; then
        info_msg "Apache is installed on this host."
        if ask_yn "Do you want to install a reverse proxy" Yn; then
            georgex.apache.install
        fi
    elif nginx_is_installed; then
        info_msg "Nginx is installed on this host."
        if ask_yn "Do you want to install a reverse proxy" Yn; then
            georgex.nginx.install
        fi
    else
        info_msg "Don't forget to install HTTP site."
    fi
}

georgex.remove.all() {
    local redis_url

    rst_title "De-Install GeorgeX (service)"
    if ! ask_yn "Do you really want to deinstall GeorgeX?"; then
        return
    fi

    redis_url=$(georgex.instance.get_setting redis.url)
    if ! [[ ${redis_url} = unix://${REDIS_HOME}/run/redis.sock* ]]; then
        georgex.remove.redis
    fi

    georgex.remove.uwsgi
    drop_service_account "${SERVICE_USER}"
    georgex.remove.settings
    wait_key

    if service_is_available "${GEORGEX_URL}"; then
        MSG="** Don't forgett to remove your public site! (${GEORGEX_URL}) **" wait_key 10
    fi
}

georgex.install.user() {
    rst_title "GeorgeX -- install user" section
    echo
    if getent passwd "${SERVICE_USER}"  > /dev/null; then
       echo "user already exists"
       return 0
    fi

    tee_stderr 1 <<EOF | bash | prefix_stdout
useradd --shell /bin/bash --system \
 --home-dir "${SERVICE_HOME}" \
 --comment 'Privacy-respecting metasearch engine' ${SERVICE_USER}
mkdir "${SERVICE_HOME}"
chown -R "${SERVICE_GROUP}:${SERVICE_GROUP}" "${SERVICE_HOME}"
groups ${SERVICE_USER}
EOF
}

georgex.install.packages() {
    TITLE="GeorgeX -- install packages" pkg_install "${GEORGEX_PACKAGES}"
}

georgex.install.buildhost() {
    TITLE="GeorgeX -- install buildhost packages" pkg_install \
         "${GEORGEX_PACKAGES} ${GEORGEX_BUILD_PACKAGES}"
}

georgex.install.clone() {
    rst_title "Clone GeorgeX sources" section
    if ! service_account_is_available "${SERVICE_USER}"; then
        die 42 "To clone GeorgeX, first install user ${SERVICE_USER}."
    fi
    echo
    if ! sudo -i -u "${SERVICE_USER}" ls -d "$REPO_ROOT" > /dev/null; then
        die 42 "user '${SERVICE_USER}' missed read permission: $REPO_ROOT"
    fi
    # SERVICE_HOME="$(sudo -i -u "${SERVICE_USER}" echo \$HOME 2>/dev/null)"
    if [[ ! "${SERVICE_HOME}" ]]; then
        err_msg "to clone GeorgeX sources, user ${SERVICE_USER} hast to be created first"
        return 42
    fi
    if [[ ! $(git show-ref "refs/heads/${GIT_BRANCH}") ]]; then
        warn_msg "missing local branch ${GIT_BRANCH}"
        info_msg "create local branch ${GIT_BRANCH} from start point: origin/${GIT_BRANCH}"
        git branch "${GIT_BRANCH}" "origin/${GIT_BRANCH}"
    fi
    if [[ ! $(git rev-parse --abbrev-ref HEAD) == "${GIT_BRANCH}" ]]; then
        warn_msg "take into account, installing branch $GIT_BRANCH while current branch is $(git rev-parse --abbrev-ref HEAD)"
    fi
    # export SERVICE_HOME

    # clone repo and add a safe.directory entry to git's system config / see
    # https://github.com/georgex/georgex/issues/1251
    git_clone "$REPO_ROOT" "${GEORGEX_SRC}" \
              "$GIT_BRANCH" "${SERVICE_USER}"
    git config --system --add safe.directory "${GEORGEX_SRC}"

    pushd "${GEORGEX_SRC}" > /dev/null
    tee_stderr 0.1 <<EOF | sudo -H -u "${SERVICE_USER}" -i 2>&1 | prefix_stdout "$_service_prefix"
cd "${GEORGEX_SRC}"
git remote set-url origin ${GIT_URL}
git config user.email "${ADMIN_EMAIL}"
git config user.name "${ADMIN_NAME}"
git config --list
EOF
    popd > /dev/null
}

georgex.install.pyenv() {
    rst_title "Create virtualenv (python)" section
    echo
    if [[ ! -f "${GEORGEX_SRC}/manage" ]]; then
        die 42 "To create pyenv for GeorgeX, first install georgex-src."
    fi
    info_msg "create pyenv in ${GEORGEX_PYENV}"
    tee_stderr 0.1 <<EOF | sudo -H -u "${SERVICE_USER}" -i 2>&1 | prefix_stdout "$_service_prefix"
rm -rf "${GEORGEX_PYENV}"
python3 -m venv "${GEORGEX_PYENV}"
grep -qFs -- 'source ${GEORGEX_PYENV}/bin/activate' ~/.profile \
  || echo 'source ${GEORGEX_PYENV}/bin/activate' >> ~/.profile
EOF
    info_msg "inspect python's virtual environment"
    tee_stderr 0.1 <<EOF | sudo -H -u "${SERVICE_USER}" -i 2>&1 | prefix_stdout "$_service_prefix"
command -v python && python --version
EOF
    wait_key
    info_msg "install needed python packages"
    tee_stderr 0.1 <<EOF | sudo -H -u "${SERVICE_USER}" -i 2>&1 | prefix_stdout "$_service_prefix"
pip install -U pip
pip install -U setuptools
pip install -U wheel
pip install -U pyyaml
cd ${GEORGEX_SRC}
pip install -e .
EOF
}

georgex.remove.pyenv() {
    rst_title "Remove virtualenv (python)" section
    if ! ask_yn "Do you really want to drop ${GEORGEX_PYENV} ?"; then
        return
    fi
    info_msg "remove pyenv activation from ~/.profile"
    tee_stderr 0.1 <<EOF | sudo -H -u "${SERVICE_USER}" -i 2>&1 |  prefix_stdout "$_service_prefix"
grep -v 'source ${GEORGEX_PYENV}/bin/activate' ~/.profile > ~/.profile.##
mv ~/.profile.## ~/.profile
EOF
    rm -rf "${GEORGEX_PYENV}"
}

georgex.install.settings() {
    rst_title "install ${GEORGEX_SETTINGS_PATH}" section

    if ! [[ -f "${GEORGEX_SRC}/.git/config" ]]; then
        die "Before install settings, first install GeorgeX."
        exit 42
    fi

    mkdir -p "$(dirname "${GEORGEX_SETTINGS_PATH}")"

    DEFAULT_SELECT=1 \
                  install_template --no-eval \
                  "${GEORGEX_SETTINGS_PATH}" \
                  "${SERVICE_USER}" "${SERVICE_GROUP}"

    tee_stderr 0.1 <<EOF | sudo -H -i 2>&1 | prefix_stdout "root"
sed -i -e "s/ultrasecretkey/$(openssl rand -hex 16)/g" "${GEORGEX_SETTINGS_PATH}"
EOF
}

georgex.remove.settings() {
    rst_title "remove ${GEORGEX_SETTINGS_PATH}" section
    if ask_yn "Do you want to delete the GeorgeX settings?" Yn; then
        rm -f "${GEORGEX_SETTINGS_PATH}"
    fi
}

georgex.check() {
    rst_title "GeorgeX checks" section

    for NAME in "searx" "filtron" "morty"; do
        if service_account_is_available "${NAME}"; then
            err_msg "There exists an old '${NAME}' account from a previous installation."
        else
            info_msg "[OK] (old) account '${NAME}' does not exists"
        fi
    done

    "${GEORGEX_PYENV}/bin/python" "${GEORGEX_SRC}/utils/georgex_check.py"
}

georgex.instance.update() {
    rst_title "Update GeorgeX instance"
    rst_para "fetch from $GIT_URL and reset to origin/$GIT_BRANCH"
    tee_stderr 0.3 <<EOF | sudo -H -u "${SERVICE_USER}" -i 2>&1 | prefix_stdout "$_service_prefix"
cd ${GEORGEX_SRC}
git fetch origin "$GIT_BRANCH"
git reset --hard "origin/$GIT_BRANCH"
pip install -U pip
pip install -U setuptools
pip install -U wheel
pip install -U pyyaml
pip install -U -e .
EOF
    rst_para "update instance's settings.yml from ${GEORGEX_SETTINGS_PATH}"
    DEFAULT_SELECT=2 \
                  install_template --no-eval \
                  "${GEORGEX_SETTINGS_PATH}" \
                  "${SERVICE_USER}" "${SERVICE_GROUP}"

    sudo -H -i <<EOF
sed -i -e "s/ultrasecretkey/$(openssl rand -hex 16)/g" "${GEORGEX_SETTINGS_PATH}"
EOF
    uWSGI_restart "${GEORGEX_UWSGI_APP}"
}

georgex.install.uwsgi() {
    rst_title "GeorgeX (install uwsgi)"
    install_uwsgi
    if [[ ${GEORGEX_UWSGI_USE_SOCKET} == true ]]; then
        georgex.install.uwsgi.socket
    else
        georgex.install.uwsgi.http
    fi
}

georgex.install.uwsgi.http() {
    rst_para "Install ${GEORGEX_UWSGI_APP} at: http://${GEORGEX_INTERNAL_HTTP}"
    uWSGI_install_app "${GEORGEX_UWSGI_APP}"
    if ! georgex.uwsgi.available; then
        err_msg "URL http://${GEORGEX_INTERNAL_HTTP} not available, check GeorgeX & uwsgi setup!"
    fi
}

georgex.install.uwsgi.socket() {
    rst_para "Install ${GEORGEX_UWSGI_APP} using socket at: ${GEORGEX_UWSGI_SOCKET}"
    mkdir -p "$(dirname ${GEORGEX_UWSGI_SOCKET})"
    chown -R "${SERVICE_USER}:${SERVICE_GROUP}" "$(dirname ${GEORGEX_UWSGI_SOCKET})"

    case $DIST_ID-$DIST_VERS in
        fedora-*)
            # Fedora runs uWSGI in emperor-tyrant mode: in Tyrant mode the
            # Emperor will run the vassal using the UID/GID of the vassal
            # configuration file [1] (user and group of the app .ini file).
            # [1] https://uwsgi-docs.readthedocs.io/en/latest/Emperor.html#tyrant-mode-secure-multi-user-hosting
            uWSGI_install_app --variant=socket  "${GEORGEX_UWSGI_APP}" "${SERVICE_USER}" "${SERVICE_GROUP}"
            ;;
        *)
            uWSGI_install_app --variant=socket  "${GEORGEX_UWSGI_APP}"
            ;;
    esac
    sleep 5
    if ! georgex.uwsgi.available; then
        err_msg "uWSGI socket not available at: ${GEORGEX_UWSGI_SOCKET}"
    fi
}

georgex.uwsgi.available() {
    if [[ ${GEORGEX_UWSGI_USE_SOCKET} == true ]]; then
        [[ -S "${GEORGEX_UWSGI_SOCKET}" ]]
        exit_val=$?
        if [[ $exit_val = 0 ]]; then
            info_msg "uWSGI socket is located at: ${GEORGEX_UWSGI_SOCKET}"
        fi
    else
        service_is_available "http://${GEORGEX_INTERNAL_HTTP}"
        exit_val=$?
    fi
    return "$exit_val"
}

georgex.remove.uwsgi() {
    rst_title "Remove GeorgeX's uWSGI app (${GEORGEX_UWSGI_APP})" section
    echo
    uWSGI_remove_app "${GEORGEX_UWSGI_APP}"
}

georgex.install.redis() {
    rst_title "GeorgeX (install redis)"
    redis.build
    redis.install
    redis.addgrp "${SERVICE_USER}"
}

georgex.remove.redis() {
    rst_title "GeorgeX (remove redis)"
    redis.rmgrp "${SERVICE_USER}"
    redis.remove
}

georgex.instance.localtest() {
    rst_title "Test GeorgeX instance localy" section
    rst_para "Activate debug mode, start a minimal GeorgeX "\
             "service and debug a HTTP request/response cycle."

    if service_is_available "http://${GEORGEX_INTERNAL_HTTP}" &>/dev/null; then
        err_msg "URL/port http://${GEORGEX_INTERNAL_HTTP} is already in use, you"
        err_msg "should stop that service before starting local tests!"
        if ! ask_yn "Continue with local tests?"; then
            return
        fi
    fi
    echo
    georgex.instance.debug.on
    tee_stderr 0.1 <<EOF | sudo -H -u "${SERVICE_USER}" -i 2>&1 |  prefix_stdout "$_service_prefix"
export GEORGEX_SETTINGS_PATH="${GEORGEX_SETTINGS_PATH}"
cd ${GEORGEX_SRC}
timeout 10 python searx/webapp.py &
sleep 3
curl --location --verbose --head --insecure ${GEORGEX_INTERNAL_HTTP}
EOF
    echo
    georgex.instance.debug.off
}

georgex.install.http.pre() {
    if ! georgex.uwsgi.available; then
        rst_para "\
To install uWSGI use::

    $(basename "$0") install uwsgi
"
        die 42 "GeorgeX's uWSGI app not available"
    fi

    if ! georgex.instance.exec python -c "from searx.shared import redisdb; redisdb.init() or exit(42)"; then
        rst_para "\
The configured redis DB is not available: If your server is public to the
internet, you should setup a bot protection to block excessively bot queries.
Bot protection requires a redis DB.  About bot protection visit the official
GeorgeX documentation and query for the word 'limiter'.
"
    fi
}

georgex.apache.install() {
    rst_title "Install Apache site ${APACHE_GEORGEX_SITE}"
    rst_para "\
This installs GeorgeX's uWSGI app as apache site.  The apache site is located at:
${APACHE_SITES_AVAILABLE}/${APACHE_GEORGEX_SITE}."
    georgex.install.http.pre

    if ! apache_is_installed; then
        err_msg "Apache packages are not installed"
        if ! ask_yn "Do you really want to continue and install apache packages?" Yn; then
            return
        else
            FORCE_SELECTION=Y install_apache
        fi
    else
        info_msg "Apache packages are installed [OK]"
    fi

    if [[ ${GEORGEX_UWSGI_USE_SOCKET} == true ]]; then
        apache_install_site --variant=socket "${APACHE_GEORGEX_SITE}"
    else
        apache_install_site "${APACHE_GEORGEX_SITE}"
    fi

    if ! service_is_available "${GEORGEX_URL}"; then
        err_msg "Public service at ${GEORGEX_URL} is not available!"
    fi
}

georgex.apache.remove() {
    rst_title "Remove Apache site ${APACHE_GEORGEX_SITE}"
    rst_para "\
This removes apache site ${APACHE_GEORGEX_SITE}::

  ${APACHE_SITES_AVAILABLE}/${APACHE_GEORGEX_SITE}"

    ! apache_is_installed && err_msg "Apache is not installed."
    if ! ask_yn "Do you really want to continue?" Yn; then
        return
    fi
    apache_remove_site "${APACHE_GEORGEX_SITE}"
}

georgex.nginx.install() {

    rst_title "Install nginx site ${NGINX_GEORGEX_SITE}"
    rst_para "\
This installs GeorgeX's uWSGI app as Nginx site.  The Nginx site is located at:
${NGINX_APPS_AVAILABLE}/${NGINX_GEORGEX_SITE} and requires a uWSGI."
    georgex.install.http.pre

    if ! nginx_is_installed ; then
        err_msg "Nginx packages are not installed"
        if ! ask_yn "Do you really want to continue and install Nginx packages?" Yn; then
            return
        else
            FORCE_SELECTION=Y install_nginx
        fi
    else
        info_msg "Nginx packages are installed [OK]"
    fi

    if [[ ${GEORGEX_UWSGI_USE_SOCKET} == true ]]; then
        nginx_install_app --variant=socket "${NGINX_GEORGEX_SITE}"
    else
        nginx_install_app "${NGINX_GEORGEX_SITE}"
    fi

    if ! service_is_available "${GEORGEX_URL}"; then
        err_msg "Public service at ${GEORGEX_URL} is not available!"
    fi
}

georgex.nginx.remove() {
    rst_title "Remove Nginx site ${NGINX_GEORGEX_SITE}"
    rst_para "\
This removes Nginx site ${NGINX_GEORGEX_SITE}::

  ${NGINX_APPS_AVAILABLE}/${NGINX_GEORGEX_SITE}"

    ! nginx_is_installed && err_msg "Nginx is not installed."
    if ! ask_yn "Do you really want to continue?" Yn; then
        return
    fi
    nginx_remove_app "${NGINX_GEORGEX_SITE}"
}

georgex.instance.exec() {
    if ! service_account_is_available "${SERVICE_USER}"; then
        die 42 "can't execute: instance does not exists (missed account ${SERVICE_USER})"
    fi
    sudo -H -i -u "${SERVICE_USER}" \
         GEORGEX_UWSGI_USE_SOCKET="${GEORGEX_UWSGI_USE_SOCKET}" \
         "$@"
}

georgex.instance.self.call() {
    # wrapper to call a function in instance's environment
    info_msg "wrapper:  utils/georgex.sh instance _call $*"
    georgex.instance.exec "${GEORGEX_SRC}/utils/georgex.sh" instance _call "$@"
}

georgex.instance.get_setting() {
    georgex.instance.exec python <<EOF
from searx import get_setting
print(get_setting('$1'))
EOF
}

georgex.instance.debug.on() {
    warn_msg "Do not enable debug in a production environment!"
    info_msg "try to enable debug mode ..."
    tee_stderr 0.1 <<EOF | sudo -H -i 2>&1 |  prefix_stdout "$_service_prefix"
cd ${GEORGEX_SRC}
sed -i -e "s/debug: false/debug: true/g" "$GEORGEX_SETTINGS_PATH"
EOF
    uWSGI_restart "$GEORGEX_UWSGI_APP"
}

georgex.instance.debug.off() {
    info_msg "try to disable debug mode ..."
    tee_stderr 0.1 <<EOF | sudo -H -i 2>&1 |  prefix_stdout "$_service_prefix"
cd ${GEORGEX_SRC}
sed -i -e "s/debug: true/debug: false/g" "$GEORGEX_SETTINGS_PATH"
EOF
    uWSGI_restart "$GEORGEX_UWSGI_APP"
}

georgex.instance.inspect() {
    rst_title "Inspect GeorgeX instance"
    echo

    georgex.instance.self.call _georgex.instance.inspect

    local _debug_on
    if ask_yn "Enable GeorgeX debug mode?"; then
        georgex.instance.debug.on
        _debug_on=1
    fi
    echo

    case $DIST_ID-$DIST_VERS in
        ubuntu-*|debian-*)
            # For uWSGI debian uses the LSB init process; for each configuration
            # file new uWSGI daemon instance is started with additional option.
            service uwsgi status "${SERVICE_NAME}"
            ;;
        arch-*)
            systemctl --no-pager -l status "uwsgi@${SERVICE_NAME%.*}"
            ;;
        fedora-*)
            systemctl --no-pager -l status uwsgi
            ;;
    esac

    echo -e  "// use ${_BCyan}CTRL-C${_creset} to stop monitoring the log"
    read -r -s -n1 -t 5
    echo

    while true;  do
        trap break 2
        case $DIST_ID-$DIST_VERS in
            ubuntu-*|debian-*) tail -f "/var/log/uwsgi/app/${SERVICE_NAME%.*}.log" ;;
            arch-*)  journalctl -f -u "uwsgi@${SERVICE_NAME%.*}" ;;
            fedora-*)  journalctl -f -u uwsgi ;;
        esac
    done

    if [[ $_debug_on == 1 ]]; then
        georgex.instance.debug.off
    fi
    return 0
}

_georgex.instance.inspect() {
    georgex.instance.env

    if in_container; then
        # shellcheck source=utils/lxc-georgex.env
        source "${REPO_ROOT}/utils/lxc-georgex.env"
        lxc_suite_info
    fi

    MSG="${_Green}[${_BCyan}CTRL-C${_Green}] to stop or [${_BCyan}KEY${_Green}] to continue${_creset}"

    if ! georgex.uwsgi.available; then
        err_msg "GeorgeX's uWSGI app not available"
        wait_key
    fi
    if ! service_is_available "${GEORGEX_URL}"; then
        err_msg "Public service at ${GEORGEX_URL} is not available!"
        wait_key
    fi
}

georgex.doc.rst() {
    local debian="${GEORGEX_PACKAGES_debian}"
    local arch="${GEORGEX_PACKAGES_arch}"
    local fedora="${GEORGEX_PACKAGES_fedora}"
    local debian_build="${GEORGEX_BUILD_PACKAGES_debian}"
    local arch_build="${GEORGEX_BUILD_PACKAGES_arch}"
    local fedora_build="${GEORGEX_BUILD_PACKAGES_fedora}"
    debian="$(echo "${debian}" | sed 's/.*/          & \\/' | sed '$ s/.$//')"
    arch="$(echo "${arch}"     | sed 's/.*/          & \\/' | sed '$ s/.$//')"
    fedora="$(echo "${fedora}" | sed 's/.*/          & \\/' | sed '$ s/.$//')"
    debian_build="$(echo "${debian_build}" | sed 's/.*/          & \\/' | sed '$ s/.$//')"
    arch_build="$(echo "${arch_build}"     | sed 's/.*/          & \\/' | sed '$ s/.$//')"
    fedora_build="$(echo "${fedora_build}" | sed 's/.*/          & \\/' | sed '$ s/.$//')"

    if [[ ${GEORGEX_UWSGI_USE_SOCKET} == true ]]; then
        uwsgi_variant=':socket'
    else
        uwsgi_variant=':socket'
    fi

    eval "echo \"$(< "${REPO_ROOT}/docs/build-templates/georgex.rst")\""

    # I use ubuntu-20.04 here to demonstrate that versions are also supported,
    # normally debian-* and ubuntu-* are most the same.

    for DIST_NAME in ubuntu-20.04 arch fedora; do
        (
            DIST_ID=${DIST_NAME%-*}
            DIST_VERS=${DIST_NAME#*-}
            [[ $DIST_VERS =~ $DIST_ID ]] && DIST_VERS=
            uWSGI_distro_setup

            echo -e "\n.. START georgex uwsgi-description $DIST_NAME"

            case $DIST_ID-$DIST_VERS in
                ubuntu-*|debian-*)  cat <<EOF

.. code:: bash

   # init.d --> /usr/share/doc/uwsgi/README.Debian.gz
   # For uWSGI debian uses the LSB init process, this might be changed
   # one day, see https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=833067

   create     ${uWSGI_APPS_AVAILABLE}/${GEORGEX_UWSGI_APP}
   enable:    sudo -H ln -s ${uWSGI_APPS_AVAILABLE}/${GEORGEX_UWSGI_APP} ${uWSGI_APPS_ENABLED}/
   start:     sudo -H service uwsgi start   ${GEORGEX_UWSGI_APP%.*}
   restart:   sudo -H service uwsgi restart ${GEORGEX_UWSGI_APP%.*}
   stop:      sudo -H service uwsgi stop    ${GEORGEX_UWSGI_APP%.*}
   disable:   sudo -H rm ${uWSGI_APPS_ENABLED}/${GEORGEX_UWSGI_APP}

EOF
                ;;
                arch-*) cat <<EOF

.. code:: bash

   # systemd --> /usr/lib/systemd/system/uwsgi@.service
   # For uWSGI archlinux uses systemd template units, see
   # - http://0pointer.de/blog/projects/instances.html
   # - https://uwsgi-docs.readthedocs.io/en/latest/Systemd.html#one-service-per-app-in-systemd

   create:    ${uWSGI_APPS_ENABLED}/${GEORGEX_UWSGI_APP}
   enable:    sudo -H systemctl enable   uwsgi@${GEORGEX_UWSGI_APP%.*}
   start:     sudo -H systemctl start    uwsgi@${GEORGEX_UWSGI_APP%.*}
   restart:   sudo -H systemctl restart  uwsgi@${GEORGEX_UWSGI_APP%.*}
   stop:      sudo -H systemctl stop     uwsgi@${GEORGEX_UWSGI_APP%.*}
   disable:   sudo -H systemctl disable  uwsgi@${GEORGEX_UWSGI_APP%.*}

EOF
                ;;
                fedora-*|centos-7) cat <<EOF

.. code:: bash

   # systemd --> /usr/lib/systemd/system/uwsgi.service
   # The unit file starts uWSGI in emperor mode (/etc/uwsgi.ini), see
   # - https://uwsgi-docs.readthedocs.io/en/latest/Emperor.html

   create:    ${uWSGI_APPS_ENABLED}/${GEORGEX_UWSGI_APP}
   restart:   sudo -H touch ${uWSGI_APPS_ENABLED}/${GEORGEX_UWSGI_APP}
   disable:   sudo -H rm ${uWSGI_APPS_ENABLED}/${GEORGEX_UWSGI_APP}

EOF
                ;;
            esac
            echo -e ".. END georgex uwsgi-description $DIST_NAME"

            local _show_cursor=""  # prevent from prefix_stdout's trailing show-cursor

            echo -e "\n.. START georgex uwsgi-appini $DIST_NAME"
            echo ".. code:: bash"
            echo
            eval "echo \"$(< "${TEMPLATES}/${uWSGI_APPS_AVAILABLE}/${GEORGEX_UWSGI_APP}${uwsgi_variant}")\"" | prefix_stdout "  "
            echo -e "\n.. END georgex uwsgi-appini $DIST_NAME"

            echo -e "\n.. START nginx socket"
            echo ".. code:: nginx"
            echo
            eval "echo \"$(< "${TEMPLATES}/${NGINX_APPS_AVAILABLE}/${NGINX_GEORGEX_SITE}:socket")\"" | prefix_stdout "  "
            echo -e "\n.. END nginx socket"

            echo -e "\n.. START nginx http"
            echo ".. code:: nginx"
            echo
            eval "echo \"$(< "${TEMPLATES}/${NGINX_APPS_AVAILABLE}/${NGINX_GEORGEX_SITE}")\"" | prefix_stdout "  "
            echo -e "\n.. END nginx http"

            echo -e "\n.. START apache socket"
            echo ".. code:: apache"
            echo
            eval "echo \"$(< "${TEMPLATES}/${APACHE_SITES_AVAILABLE}/${APACHE_GEORGEX_SITE}:socket")\"" | prefix_stdout "  "
            echo -e "\n.. END apache socket"

            echo -e "\n.. START apache http"
            echo ".. code:: apache"
            echo
            eval "echo \"$(< "${TEMPLATES}/${APACHE_SITES_AVAILABLE}/${APACHE_GEORGEX_SITE}")\"" | prefix_stdout "  "
            echo -e "\n.. END apache http"
        )
    done

}

# ----------------------------------------------------------------------------
main "$@"
# ----------------------------------------------------------------------------
