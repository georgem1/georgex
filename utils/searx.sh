#!/usr/bin/env bash
# SPDX-License-Identifier: AGPL-3.0-or-later
# shellcheck disable=SC2001

# shellcheck source=utils/lib.sh
source "$(dirname "${BASH_SOURCE[0]}")/lib.sh"
# shellcheck source=utils/brand.env
source "${REPO_ROOT}/utils/brand.env"

# ----------------------------------------------------------------------------
# config
# ----------------------------------------------------------------------------

PUBLIC_URL="${PUBLIC_URL:-${GEORGEX_URL}}"

SERVICE_NAME="searx"
SERVICE_USER="${SERVICE_USER:-${SERVICE_NAME}}"
GEORGEX_SETTINGS_PATH="/etc/searx/settings.yml"
GEORGEX_UWSGI_APP="searx.ini"

# ----------------------------------------------------------------------------
usage() {
# ----------------------------------------------------------------------------

    # shellcheck disable=SC1117
    cat <<EOF
usage::
  $(basename "$0") remove     all

remove all:    complete uninstall of GeorgeX service

environment:
  PUBLIC_URL   : ${PUBLIC_URL}
EOF

    [[ -n ${1} ]] &&  err_msg "$1"
}

main() {

    local _usage="unknown or missing $1 command $2"

    case $1 in
        remove)
            rst_title "GeorgeX (remove)" part
            sudo_or_exit
            case $2 in
                all) remove_all;;
                *) usage "$_usage"; exit 42;;
            esac ;;
        *) usage "unknown or missing command $1"; exit 42;;
    esac
}

remove_all() {
    rst_title "De-Install GeorgeX (service)"

    rst_para "\
It goes without saying that this script can only be used to remove
installations that were installed with this script."

    if ! ask_yn "Do you really want to deinstall GeorgeX?"; then
        return
    fi
    remove_searx_uwsgi
    drop_service_account "${SERVICE_USER}"
    remove_settings
    wait_key
    if service_is_available "${PUBLIC_URL}"; then
        MSG="** Don't forgett to remove your public site! (${PUBLIC_URL}) **" wait_key 10
    fi
}

remove_settings() {
    rst_title "remove GeorgeX settings" section
    echo
    info_msg "delete ${GEORGEX_SETTINGS_PATH}"
    rm -f "${GEORGEX_SETTINGS_PATH}"
}

remove_searx_uwsgi() {
    rst_title "Remove GeorgeX's uWSGI app (georgex.ini)" section
    echo
    uWSGI_remove_app "$GEORGEX_UWSGI_APP"
}


# ----------------------------------------------------------------------------
main "$@"
# ----------------------------------------------------------------------------
